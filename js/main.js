const engWord = document.getElementById('eng'),
      rusWord = document.getElementById('rus'),
      inputs = document.getElementsByClassName('input'),
      addButton = document.getElementById('add-word-btn'),
      table = document.getElementById('table');

let words;
let btnsDelete;

// при первом заходе на страницу words будет пустым массивом. Если мы уже сохраняли слова в LocalStorage, то они сразу попадут в переменную words
localStorage.length < 1 ? words = [] : words = JSON.parse(localStorage.getItem('words'));

const addEventDelete = () => {
    if (words.length > 0) {
        btnsDelete = document.querySelectorAll('.btn-delete');
        for (let btn of btnsDelete) {
            btn.addEventListener('click', e => {
                deleteWord(e);
            });
        }
    }
}

const addWordToTable = index => {
    table.innerHTML += `
        <tr class="tr">
            <td class="eng-word">${words[index].english}</td>
            <td class="rus-word">${words[index].russian}</td>
            <td>
                <button class="btn-delete"></button>
            </td>
        </tr>
    `;
    addEventDelete();
}

// первый параметр всегда один элемент, даже если он не используется
words.forEach((word, i) => {
    addWordToTable(i);
});

addButton.addEventListener('click', () => {
    if (
            engWord.value.length < 1 ||
            rusWord.value.length < 1 ||
            !isNaN(engWord.value) ||
            !isNaN(rusWord.value)
        ){
            for (let key of inputs) {
                key.classList.add('error');
            }
        } else {
            for (let key of inputs) {
                key.classList.remove('error');
            }
            words.push(new CraeteWord(engWord.value, rusWord.value));
            localStorage.setItem('words', JSON.stringify(words));
            addWordToTable(words.length - 1);
            engWord.value = null;
            rusWord.value = null;
        }
})

// конструктор объектов (слово + перевод)
function CraeteWord (english, russian) {
    this.english = english;
    this.russian = russian;
}

const deleteWord = e => {
    // покажет на какой по счёту tr мы кликнули (начиная с 0)
    const rowIndex = e.target.parentNode.parentNode.rowIndex; // FIXME: костылямба
    // удаляем вместе с tbody
    e.target.parentNode.parentNode.parentNode.remove(); // FIXME: костылямба
    // удаляем 1 элемент начиная с rowIndex 
    words.splice(rowIndex, 1);
    // очищаем поле words в localStorage и добавляем заново измененный
    localStorage.removeItem('words');
    localStorage.setItem('words', JSON.stringify(words));
}

// addEventDelete();